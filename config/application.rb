require 'uri'
require 'em-synchrony/activerecord'
require 'yaml'
require 'erb'

ENV['RACK_ENV'] ||= "test"
Bundler.require :default, ENV['RACK_ENV']

module Db
  mattr_accessor :config
  self.config ||= YAML.load(ERB.new(File.read('config/database.yml')).result)
end

environment = ENV['RACK_ENV']
db = Db.config[environment]
ActiveRecord::Base.establish_connection(db)



