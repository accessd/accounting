require 'rubygems'
require 'bundler/setup'
require 'goliath'
require 'em-synchrony/activerecord'
require 'grape'

require './config/application'
require './app/services/keystone/request'
require './app/services/keystone/models/users'
require './app/models/usage'
require './app/models/user'
require './app/services/users_manager'
require './app/apis/users'

class Application < Goliath::API

  def response(env)
    ::Users.call(env)
  end

end