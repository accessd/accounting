class Usage < ActiveRecord::Base
  db = Db.config['billing']
  establish_connection(db)

  def self.create_billing_db(account_id:)
    raise 'Account id must provided' unless account_id
    db_name = "billing_#{account_id}"
    connection.create_database(db_name)
    db = Db.config['billing']
    db['database'] = db_name
    establish_connection(db)
  end

  def self.create_table
    connection.create_table 'usage' do |t|
      t.string   :account_id, null: false
      t.datetime :date,       null: false
      t.decimal  :amount,     default: 0.0, null: false
      t.integer  :bill_mode,  null: false
    end
  end
end