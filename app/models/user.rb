class User < ActiveRecord::Base
  validates :name, :password, :account_id, :group_id, presence: true
  validates :account_id, uniqueness: {scope: :group_id}
end