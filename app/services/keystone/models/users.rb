module Keystone
  class Users
    include Request
    attr_accessor :id, :tenant_id, :name, :password

    def initialize(id: nil, tenant_id: nil)
      self.id = id
      self.tenant_id = tenant_id
    end

    def find
      return if id.nil? || id.empty?

      self.response = request(method: 'GET', path: "/users/#{id}")
      return if response_code_is?(NOT_FOUND_RESPONSE_CODE)
      response
    end

    def save
      data = {
        user: {
          name: name,
          password: password,
          tenantId: tenant_id
        }
      }
      self.response = request(method: 'POST', path: '/users', body: data.to_json)
      return false if response_code_is?(BAD_REQUEST_RESPONSE_CODE)
      response.try(:[], :user)
    end

    def authenticate
      data = {
        auth: { 
          passwordCredentials: {
            username: name,
            password: password
          },
          tenantId: tenant_id
        }
      }
      self.response = request(method: 'POST', path: '/tokens', body: data.to_json)
      return false if response_code_is?(BAD_REQUEST_RESPONSE_CODE)
      response
    end

    private

    attr_accessor :response

    def response_code_is?(code)
      response.try(:[], :error).try(:[], :code) == code
    end
  end
end