require 'em-synchrony/em-http'

module Keystone
  MANAGEMENT_URL = 'http://192.168.33.33:35357/v2.0'
  AUTH_TOKEN = 'ADMIN'

  module Request
    ALLOWED_METHODS = ['GET', 'POST', 'DELETE', 'PUT']
    NOT_FOUND_RESPONSE_CODE = 404
    BAD_REQUEST_RESPONSE_CODE = 400

    def request(method:, path:, body: '')
      raise 'Not allowed request method' unless ALLOWED_METHODS.include?(method)

      http = EventMachine::HttpRequest.new("#{MANAGEMENT_URL}#{path}").public_send(method.downcase,
        head: {'X-Auth-Token' => 'ADMIN', 'Content-Type' => 'application/json', 'User-Agent' => 'Accounting API'},
        body: body)
      symbolize_keys(JSON.parse(http.response))
    end

    # Helper method to recursively symbolize hash keys.
    def symbolize_keys(obj)
      case obj
      when Array
        obj.inject([]){|res, val|
          res << case val
          when Hash, Array
            symbolize_keys(val)
          else
            val
          end
          res
        }
      when Hash
        obj.inject({}){|res, (key, val)|
          nkey = case key
          when String
            key.to_sym
          else
            key
          end
          nval = case val
          when Hash, Array
            symbolize_keys(val)
          else
            val
          end
          res[nkey] = nval
          res
        }
      else
        obj
      end
    end
  end
end
