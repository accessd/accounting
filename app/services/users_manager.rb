require 'securerandom'
class KeystoneUserCreateError < StandardError;end
class LocalUserCreateError < StandardError;end

class UsersManager
  attr_accessor :keystone_user, :local_user

  def initialize(keystone_user:, local_user:)
    self.keystone_user = keystone_user
    self.local_user = local_user
    self.keystone_user.name = (local_user.name || generate_local_user_name)
    self.keystone_user.password = (local_user.password || generate_local_user_password)
  end

  def authenticate_user
    keystone_user_exists = keystone_user.find

    unless keystone_user_exists
      if user = keystone_user.save
        self.local_user.account_id = user[:id]
        local_user.save || raise(LocalUserCreateError, 'Could not save user to local db')
        create_billing_db
      else
        raise KeystoneUserCreateError, 'Could not save user to keystone'
      end
    end

    keystone_user.authenticate
  end

  private

  def generate_local_user_name
    self.local_user.name = "user_#{SecureRandom.hex(20)}"
  end

  def generate_local_user_password
    self.local_user.password = SecureRandom.hex(20)
  end

  def create_billing_db
    Usage.create_billing_db(account_id: local_user.account_id)
    Usage.create_table
  end
end