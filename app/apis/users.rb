class Users < Grape::API
  format :json

  rescue_from KeystoneUserCreateError, LocalUserCreateError do |e|
    rack_response({ error: e.class.name, message: e.message }.to_json, 422)
  end

  rescue_from :all do |e|
    rack_response({ error: "Unexpected error: #{e.class.name}", message: e.message }.to_json)
  end
  
  resource :tokens do
    desc "Returns access token"
    params do
      requires :group_id, type: String, desc: "Group"
    end
    get do
      keystone_user = Keystone::Users.new(id: params[:account_id], tenant_id: params[:group_id])
      local_user = User.find_or_initialize_by(account_id: params[:account_id], group_id: params[:group_id])
      users_manager = UsersManager.new(keystone_user: keystone_user, local_user: local_user)
      users_manager.authenticate_user
    end
  end
end