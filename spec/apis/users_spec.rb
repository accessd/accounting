require 'spec_helper'

describe Users do
  before do
    WebMock.disable_net_connect!(allow_localhost: true)
    allow_any_instance_of(UsersManager).to receive(:create_billing_db)
  end

  describe "api" do
    it "перехватывает и возвращает описание исключения" do
      allow_any_instance_of(UsersManager).to receive(:authenticate_user) do
        raise KeystoneUserCreateError, 'Could not save user to keystone'
      end

      with_api Application do
        get_request(path: "/tokens?group_id=82f1213b78cf4077a0a23001857fb086") do |async|
          expect(async.response).to eq("{\"error\":\"KeystoneUserCreateError\",\"message\":\"Could not save user to keystone\"}")
        end
      end
    end

    describe "получение токена" do
      it "должен быть задан group_id" do
        with_api Application do
          get_request(path: "/tokens") do |async|
            expect(async.response).to include_json('group_id is missing'.to_json)
          end
        end
      end

      it "возвращает токен когда пользователя нет" do
        with_api Application do
          VCR.use_cassette('api_get_token_user_do_not_exists') do
            get_request(path: "/tokens?account_id=321&group_id=82f1213b78cf4077a0a23001857fb086") do |async|
              expect(async.response).to include('access')
            end
          end
        end
      end

      it "возвращает токен когда пользователь есть" do
        with_api Application do
          VCR.use_cassette('api_get_token_user_exists') do
            get_request(path: "/tokens?account_id=474422e1567a4c908fbfe8cf66c7ce62&group_id=82f1213b78cf4077a0a23001857fb086") do |async|
              expect(async.response).to include('access')
            end
          end
        end
      end
    end
  end
end
