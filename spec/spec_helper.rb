require 'rubygems'

ENV["RACK_ENV"] ||= 'test'

require 'rack/test'
require 'webmock/rspec'
require 'goliath/test_helper'

require File.expand_path("../../config/application", __FILE__)
require File.expand_path("../../server", __FILE__)

I18n.enforce_available_locales = false

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.include JsonSpec::Helpers
  config.include Goliath::TestHelper, file_path: /spec\/apis/

  config.mock_with :rspec
  config.expect_with :rspec

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end

VCR.configure do |c|
  c.cassette_library_dir = "spec/vcr_cassettes"
  c.hook_into :webmock
  c.ignore_localhost = true
end

Dir[File.expand_path('../factories/*.rb', __FILE__)].each do |f|
  require f
end

# Helper method to recursively symbolize hash keys.
def symbolize_keys(obj)
  case obj
  when Array
    obj.inject([]){|res, val|
      res << case val
      when Hash, Array
        symbolize_keys(val)
      else
        val
      end
      res
    }
  when Hash
    obj.inject({}){|res, (key, val)|
      nkey = case key
      when String
        key.to_sym
      else
        key
      end
      nval = case val
      when Hash, Array
        symbolize_keys(val)
      else
        val
      end
      res[nkey] = nval
      res
    }
  else
    obj
  end
end
