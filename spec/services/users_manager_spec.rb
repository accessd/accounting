require 'spec_helper'

describe UsersManager do
  before do
    WebMock.disable_net_connect!
  end

  describe "#initialize" do
    it "keystone_user name и password из local_user, если local_user есть в базе" do
      user = create(:user, name: 'Joe', password: 'qwerty')
      users_manager = UsersManager.new(keystone_user: Keystone::Users.new, local_user: user)

      expect(users_manager.keystone_user.name).to eq('Joe')
      expect(users_manager.keystone_user.password).to eq('qwerty')
    end

    it "name и password генерятся для keystone_user и local_user, если local_user нет в базе" do
      allow(SecureRandom).to receive(:hex).and_return('a2b3c4')
      users_manager = UsersManager.new(keystone_user: Keystone::Users.new, local_user: User.new)

      expect(users_manager.keystone_user.name).to eq('user_a2b3c4')
      expect(users_manager.keystone_user.password).to eq('a2b3c4')

      expect(users_manager.local_user.name).to eq('user_a2b3c4')
      expect(users_manager.local_user.password).to eq('a2b3c4')
    end
  end

  describe "#authenticate_user" do
    describe "пользователя нет в keystone" do
      before do
        @keystone_user_id = '123'
        @group_id = 'q2w3e4b5'
        @keystone_user = Keystone::Users.new(id: @keystone_user_id)
      end
      
      it "создает пользователя в keystone" do
        allow_create_billing_db

        local_user = User.new
        users_manager = UsersManager.new(keystone_user: @keystone_user, local_user: local_user)

        allow_authenticate
        allow(local_user).to receive(:save).and_return(true)

        expect(@keystone_user).to receive(:find).and_return(nil)
        expect(@keystone_user).to receive(:save).and_return(id: @keystone_user_id)
        
        users_manager.authenticate_user
      end

      it "KeystoneUserCreateError если не удалось сохранить пользователя в keystone" do
        users_manager = UsersManager.new(keystone_user: @keystone_user, local_user: User.new)

        allow(@keystone_user).to receive(:find).and_return(nil)
        allow(@keystone_user).to receive(:save).and_return(false)

        expect{ users_manager.authenticate_user }.to raise_error(KeystoneUserCreateError)
      end

      it "создает пользователя в локальной бд" do
        allow_create_billing_db

        local_user = User.new(group_id: @group_id)
        users_manager = UsersManager.new(keystone_user: @keystone_user, local_user: local_user)

        allow_authenticate
        allow_to_save_user_to_keystone
        
        users_manager.authenticate_user

        expect(User.find_by(account_id: @keystone_user_id, group_id: @group_id)).to be
      end

      it "LocalUserCreateError если не удалось сохранить пользователя в локальную бд" do
        local_user = User.new(group_id: @group_id)
        users_manager = UsersManager.new(keystone_user: @keystone_user, local_user: local_user)

        allow_to_save_user_to_keystone
        allow(local_user).to receive(:save).and_return(false)

        expect{ users_manager.authenticate_user }.to raise_error(LocalUserCreateError)
      end

      it "создает базу для биллинга" do
        User.delete_all
        local_user = User.new(group_id: @group_id)
        users_manager = UsersManager.new(keystone_user: @keystone_user, local_user: local_user)

        allow_authenticate
        allow_to_save_user_to_keystone

        expect(users_manager).to receive(:create_billing_db)
        
        users_manager.authenticate_user
      end
    end
  end

  def allow_authenticate
    allow(@keystone_user).to receive(:authenticate)
  end

  def allow_create_billing_db
    allow_any_instance_of(UsersManager).to receive(:create_billing_db)
  end

  def allow_to_save_user_to_keystone
    allow(@keystone_user).to receive(:find).and_return(nil)
    allow(@keystone_user).to receive(:save).and_return(id: @keystone_user_id)
  end
end
