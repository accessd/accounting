require 'spec_helper'

describe Keystone::Users do
  before do
    WebMock.disable_net_connect!(allow_localhost: true)
  end

  describe "#find" do
    it "возвращает nil если не передан id" do
      keystone_user = Keystone::Users.new
      expect(keystone_user.find).to be_nil
    end

    it "возвращает nil если пользователь не найден в keystone" do
      stub_request(:get, "#{Keystone::MANAGEMENT_URL}/users/123").to_return(body: "{\"error\":{\"code\":404}}")
      keystone_user = Keystone::Users.new(id: '123', tenant_id: '123')
      em_run { expect(keystone_user.find).to be_nil }
    end

    it "возвращает пользователя из keystone" do
      user_id = 'd56d2fd266a04ac2bd6b8ea0534a2a52'
      user_json = "{\"user\": {\"name\": \"test_796fa8f14de6b22a09507d385b870af6c5d9d1e0\", \"enabled\": true, \"id\": \"d56d2fd266a04ac2bd6b8ea0534a2a52\", \"tenantId\": \"82f1213b78cf4077a0a23001857fb086\"}}"
      stub_request(:get, "#{Keystone::MANAGEMENT_URL}/users/#{user_id}").to_return(body: user_json)
      keystone_user = Keystone::Users.new(id: user_id, tenant_id: '82f1213b78cf4077a0a23001857fb086')
      em_run { expect(keystone_user.find).to eq(symbolize_keys(JSON.parse(user_json))) }
    end
  end

  describe "#save" do
    it "возвращает false если не удалось сохранить пользователя" do
      stub_request(:post, "#{Keystone::MANAGEMENT_URL}/users").to_return(body: "{\"error\":{\"code\":400}}")
      keystone_user = Keystone::Users.new(id: '123', tenant_id: '123')
      em_run { expect(keystone_user.save).to be_falsy }
    end

    it "возвращает пользователя если удалось сохранить пользователя" do
      user_json = "{\"user\": {\"name\": \"test_796fa8f14de6b22a09507d385b870af6c5d9d1e0\", \"enabled\": true, \"id\": \"d56d2fd266a04ac2bd6b8ea0534a2a52\", \"tenantId\": \"82f1213b78cf4077a0a23001857fb086\"}}"

      stub_request(:post, "#{Keystone::MANAGEMENT_URL}/users").to_return(body: user_json)
      keystone_user = Keystone::Users.new(id: '123', tenant_id: '123')
      em_run { expect(keystone_user.save).to eq(symbolize_keys(JSON.parse(user_json)["user"])) }
    end
  end

  describe "#authenticate" do
    it "возвращает false если не удалось получить токен" do
      stub_request(:post, "#{Keystone::MANAGEMENT_URL}/tokens").to_return(body: "{\"error\":{\"code\":400}}")
      keystone_user = Keystone::Users.new(id: '123', tenant_id: '123')
      em_run { expect(keystone_user.authenticate).to be_falsy }
    end

    it "возвращает токен если все ок" do
      keystone_user = Keystone::Users.new(id: '474422e1567a4c908fbfe8cf66c7ce62', tenant_id: '82f1213b78cf4077a0a23001857fb086')
      keystone_user.password = '496cacc7b6acc51a901ff2fd1646d59c2c1bec5b'
      keystone_user.name = 'user_95d25ed755da3246df94dfdd0bc9e6bfe9542cd1'
      VCR.use_cassette('authenticate') do
        em_run { 
          response = keystone_user.authenticate
          expect(response).to include(:access)
          expect(response[:access]).to include(:token)
        }
      end
    end
  end

  def em_run
    EM.run do
      Fiber.new {
        yield
      }.resume
      EM.stop
    end
  end
end
