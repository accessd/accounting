FactoryGirl.define do
  sequence(:name) {|n| "user#{n}" }

  factory :user do
    name
    password '111111'
    group_id '123'
    account_id '123'
  end
end