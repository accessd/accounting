class CreateUsers < ActiveRecord::Migration
  
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.string :account_id
      t.string :group_id

      t.timestamps
    end

    add_index :users, [:account_id, :group_id], unique: true
  end
  
end